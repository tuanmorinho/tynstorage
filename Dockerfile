FROM openjdk:20
COPY target/tynstorage-0.0.1-SNAPSHOT.jar tynstorage-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/tynstorage-0.0.1-SNAPSHOT.jar"]