package tyn.tynstorage.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.IOUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class StorageS3Service {

    @Value("${aws.s3.bucket.name}")
    private String bucketName;

    @Autowired
    private final AmazonS3 amazonS3;

    public String uploadFile(MultipartFile file) {
        final File fileObj = convertMultiPartFileToFile(file);
        final UUID fileId = UUID.randomUUID();
        final String fileName = file.getOriginalFilename() + fileId;
        amazonS3.putObject(new PutObjectRequest(bucketName, fileName, fileObj));
        fileObj.delete();
        return "File uploaded: " + fileName;
    }

    public byte[] downloadFile(String fileName) {
        final S3Object s3Obj = amazonS3.getObject(bucketName, fileName);
        final S3ObjectInputStream s3ObjInputStream = s3Obj.getObjectContent();
        try {
            return IOUtils.toByteArray(s3ObjInputStream);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public String deleteFile(String fileName) {
        amazonS3.deleteObject(bucketName, fileName);
        return fileName + "removed";
    }

    private File convertMultiPartFileToFile(MultipartFile file) {
        File convertedFile = new File(Objects.requireNonNull(file.getOriginalFilename()));
        try (FileOutputStream fos = new FileOutputStream(convertedFile)) {
            fos.write(file.getBytes());
        } catch (Exception e) {}
        return convertedFile;
    }
}
