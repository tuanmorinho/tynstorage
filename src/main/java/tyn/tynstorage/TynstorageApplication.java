package tyn.tynstorage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class TynstorageApplication {

	public static void main(String[] args) {
		SpringApplication.run(TynstorageApplication.class, args);
	}

}
