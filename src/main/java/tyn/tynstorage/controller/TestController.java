package tyn.tynstorage.controller;

import lombok.RequiredArgsConstructor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
public class TestController {

    @GetMapping("")
    public String testMicroService() {
        return "Connect success";
    }

    @GetMapping("/to-master")
    public String testMassterJwt() {


        Request request = new Request.Builder()
                .url("http://localhost:8080/test")
                .header("Authorization", "Bearer " + "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJUdWFuTFEiLCJpYXQiOjE2ODkwNjk2MzIsImV4cCI6MTY4OTE1NjAzMn0.igi29P3Fjvz3WcBhPXShmNkQBCQXFGEhLd3-gy-24ZM")
                .build();

        OkHttpClient client = new OkHttpClient();
        try (Response response = client.newCall(request).execute()) {
            if (!response.isSuccessful()) {
                throw new IOException("Unexpected code: " + response);
            }
            String body = response.body().string();
            return body;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    };
}
